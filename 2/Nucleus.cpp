#include "Nucleus.h"
#include <iostream>
using namespace std;


//initializing the data of a Nucleus with given values 

void Nucleus::init(const string dna_sequence)
{
	_DNA_strand = dna_sequence; // setting the first strand
	_complementary_DNA_strand = "";

	//intializing the second strand by the values of the first strand
	for (unsigned int i = 0; i < dna_sequence.length(); i++)  
	{
		if (dna_sequence[i] == 'A')
		{
			_complementary_DNA_strand += 'T';
		}

		else if (dna_sequence[i] == 'T')
		{
			_complementary_DNA_strand += 'A';
		}

		else if (dna_sequence[i] == 'C')
		{
			_complementary_DNA_strand += 'G';
		}

		else if (dna_sequence[i] == 'G')
		{
			_complementary_DNA_strand += 'C';
		}

		else // if a letter isnt valid, printing error and exiting wit herror code 1
		{
			cerr << "invalid character in dna strand";
			_exit(1);
		}

	}
}

/*
function returnes an rna transcript accourding to a given gene
input: a gene
output: the rna transcript
*/
string Nucleus::get_RNA_transcript(const Gene& gene) const
{
	string rna = ""; //creating rna string
	
	if (gene.is_on_complementary_dna_strand() == false) //checking which strand the gene refers
	{

		for (unsigned int i = gene.get_start(); i < gene.get_end(); i++) // running accourding to the gene
		{

			if (_DNA_strand[i] == 'A')  // adding letters by the strand data
			{
				rna += 'U';
			}

			if (_DNA_strand[i] == 'U')
			{
				rna += 'A';
			}

			if (_DNA_strand[i] == 'C')
			{
				rna += 'G';
			}

			if (_DNA_strand[i] == 'G')
			{
				rna += 'C';
			}

		}
	}
	else
	{
		for (unsigned int i = gene.get_start(); i <= gene.get_end(); i++)
		{

			if (_complementary_DNA_strand[i] == 'T')
			{
				rna += 'U';
			}
			else
			{
				rna += _complementary_DNA_strand[i];
			}

		}
	}
	return rna; // returning rna
}

/*
function returnes number of appearances of a codon in the nucleus
input: a string representing the codon
output: the number of appearances of a codon in the nucleus
*/
unsigned int Nucleus::get_num_of_codon_appearances(const string& codon) const
{
	//using the find function 
	int occurrences = 0;
	string::size_type pos = 0;
	while ((pos = _DNA_strand.find(codon, pos)) != std::string::npos) { //running until the codon doesnt exists in the position of the strand 
		occurrences++; //increasing occurrences
		pos += codon.length(); // increasing position by codon length
	}
	return occurrences; // returning occurrences
}
//complementary_DNA_strand getter
string Nucleus::get_complementary_DNA_strand()const
{
	return _complementary_DNA_strand;
}


/*
function returnes a reversed strand
input: none
output: the reversed strand
*/
string Nucleus::get_reversed_DNA_strand() const
{
	//using the reverse function
	string reversed = _DNA_strand;
	reverse(reversed.begin(), reversed.end());
	return reversed;
}

