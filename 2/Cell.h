#pragma once
#include "AminoAcid.h"
#include "Gene.h"
#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Protein.h"
#include "Ribosome.h"
using namespace std;
class Cell
{
private:
	Nucleus _nucleus;
	Ribosome _ribosome;
	Mitochondrion _mitochondrion;
	unsigned int _atp_units;
	Gene _glocus_receptor_gene;

public:
	void init(const string dna_sequence, const Gene glucose_receptor_gene);
	bool get_ATP();
	
};

