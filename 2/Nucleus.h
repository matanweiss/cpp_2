#pragma once
#include <string>
#include "Gene.h"
using namespace std;
class Nucleus
{
private:
	string _DNA_strand;
	string _complementary_DNA_strand;

public:
	void init(const std::string dna_sequence);
	string get_RNA_transcript(const Gene& gene) const;
	string get_reversed_DNA_strand() const;
	unsigned int get_num_of_codon_appearances(const string& codon) const;
	string get_complementary_DNA_strand()const;
};

