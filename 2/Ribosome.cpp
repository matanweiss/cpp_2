#include "Ribosome.h"
#include <iostream>
#include <string>
#define MIN_LEN 3
using namespace std;

#define CODON_LEN 3

/*
function returnes a protein by a given rna
input: RNA transcript
output: the appropriate protein for the given rna 
*/
Protein * Ribosome::create_protein(string &RNA_transcript) const
{
	LinkedList *Pro = new LinkedList; // creting and intelizing a new protein
	Pro->init();
	
	string codon = ""; // creating string for the codon and an amino acid
	AminoAcid *acid = new AminoAcid;
	while (RNA_transcript.length()>=CODON_LEN) //validating transcript length
	{
		codon = RNA_transcript.substr(0, MIN_LEN); // using substr with length of 3 to intelize codon
		RNA_transcript = RNA_transcript.substr(MIN_LEN); // shortening rna
		
		*acid = get_amino_acid(codon); // intelizing acid with the appropriate one for  the codon
		if (*acid == UNKNOWN) // checking if the acid is valid
		{
			// freeing memory of the nodes
			AminoAcidNode* current = Pro->get_first();
			AminoAcidNode* next;

			while (current != NULL)
			{
				next = current->get_next();
				delete(current);
				current = next;
			}
			delete(Pro);
			return nullptr;
		}
		else
		{
			Pro->add(*acid); // adding the acid to the protein if he's valid
		}
	}
	return Pro; // returning the protein
}
