#include "Mitochondrion.h"
#include "Protein.h"
#include "AminoAcid.h"
#include "Ribosome.h"
#include "Nucleus.h"

#define MIN_GLUCOSE 50

/*
function intelizes the mitochondrion
input: none
output: none
*/
void Mitochondrion::init()
{
	_glocuse_level = 0; //setting glocuse level to 0
	_has_glocuse_receptor = false; // setting glocuse receptor to false
}
/*
function checks if the mitochondrion can produce glucose 
input: a protein
output: none
*/

void Mitochondrion::insert_glucose_receptor(const Protein & protein)
{
	bool flag = true; // creating flag
	AminoAcidNode * aminoAcidNode = protein.get_first(); //intelizing an amino acit chain with the values in the protein
	// checking if the values lets the mitochondrion to produce glucose
	if (aminoAcidNode->get_data() != ALANINE)
	{
		flag = false;
	}

	else if ((aminoAcidNode=aminoAcidNode->get_next())->get_data() != LEUCINE)
	{
		flag = false;
	}

	else if ((aminoAcidNode = aminoAcidNode->get_next())->get_data() != GLYCINE)
	{
		flag = false;
	}

	else if ((aminoAcidNode = aminoAcidNode->get_next())->get_data() != HISTIDINE)
	{
		flag = false;
	}

	else if ((aminoAcidNode = aminoAcidNode->get_next())->get_data() != LEUCINE)
	{
		flag = false;
	}

	else if ((aminoAcidNode = aminoAcidNode->get_next())->get_data() != PHENYLALANINE)
	{
		flag = false;
	}

	else if ((aminoAcidNode = aminoAcidNode->get_next())->get_data() != AMINO_CHAIN_END)
	{
		flag = false;
	}

	_has_glocuse_receptor = flag; // intelizing the glocuse receptor
}


//setting the glucose level
void Mitochondrion::set_glucose(const unsigned int glocuse_units)
{
	_glocuse_level = glocuse_units;
}

// returns if the mitochondrion can produce glucose by the values in the mitochondrion
bool Mitochondrion::produceATP(const int glocuse_unit) const
{
	return (_has_glocuse_receptor && _glocuse_level >= MIN_GLUCOSE);
}




