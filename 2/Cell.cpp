#include "Cell.h"

#define MIN_GLUCOSE 50
#define MAX_ATP 100
/*
function intelizes a cell
input: the gene for glucose receptor and a dna sequence
output: none
*/
void Cell::init(const string dna_sequence, const Gene glucose_receptor_gene)
{
	_nucleus.init(dna_sequence); // intelizing 
	_mitochondrion.init();
	_glocus_receptor_gene = glucose_receptor_gene;
	_atp_units = 0;
}

/*
function returnes T/F if the cell can produce atp and intelizes atp level
input: none
output: T/F
*/
bool Cell::get_ATP()
{
	bool can_produce = false;
	string rna = _nucleus.get_RNA_transcript(_glocus_receptor_gene);
	Protein *p = new Protein;
	p = _ribosome.create_protein(rna);

	if (p != nullptr)
	{
		_mitochondrion.insert_glucose_receptor(*p);
		_mitochondrion.set_glucose(MIN_GLUCOSE);
		can_produce = _mitochondrion.produceATP(MIN_GLUCOSE);
		if (can_produce)
		{
			_atp_units = MAX_ATP;
		}
	}
	else
	{
		cerr << "Couldn't produce atp" << endl;
		_exit(1);
	}
		return can_produce;
}